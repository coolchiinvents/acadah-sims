    <!-- content -->
    <div class="app-content m-n ">
      <div ui-butterbar></div>
      <a href class="off-screen-toggle hide" data-toggle="class:off-screen" data-target=".app-aside" ></a>
      <div class="app-content-body fade-in-up">
        <!-- COPY the content from "tpl/" -->
          <div class="hbox hbox-auto-xs hbox-auto-sm" ng-init="
              app.settings.asideFolded = false; 
              app.settings.asideDock = false;
            ">
            <!-- main -->
            <div class="col">
              <!-- main header -->
              <div class="bg-light lter b-b wrapper-md">
                <div class="row">
                  <div class="col-xs-12 text-center">
                    <h1 class="m-n font-thin h3 text-black">Sign Up School</h1>
                    <small class="text-muted">Welcome to Acadah Smart School application</small>
                  </div>
                </div>              
                <div class="clearfix"></div>
              </div>
              <!-- / main header -->
              <div class="wrapper-md">

                  <div class="col-md-12">
<!--                    <div class="alert alert-success fade in widget-inner text-center">-->
<!--                        <button type="button" class="close" data-dismiss="alert">x</button>-->
<!--                        Congratulations! you have successfully activated your account     -->
<!--                    </div>-->
                      <?php

                      if(!empty($_SESSION['failed'])){?>
                          <div class="alert alert-danger fade in widget-inner">
                              <button type="button" class="close" data-dismiss="alert">x</button>
                              <?php echo $_SESSION['failed']; ?>

                          </div>
                      <?php }elseif(!empty($_SESSION['success'])){?>
                          <div class="alert alert-success fade in widget-inner">
                              <button type="button" class="close" data-dismiss="alert">x</button>
                              <?php echo $_SESSION['success'];$_SESSION['activated_status'] = 1;?>
                              <script>
                  setTimeout(function () { window.location.replace("<?php echo base_url() ?>/setup");   }, 1500);
                              </script>
                          </div>
                      <?php }
                      ?>
                  
                      
                      <div class="panel panel-default" >
                        <div class="panel-heading ">ACTIVATE SCHOOL</div>
                        <div class="panel-body text-center">
                            <?php echo validation_errors(); ?>
                          <form class="form-inline ng-pristine ng-valid" role="form" method="POST" action="<?php echo base_url(); ?>setup/validate_license" enctype="multipart/form-data">
                          <div class="form-group">
                            <div class="panel-heading ">UPLOAD PASSPORT</div>

          <div class="panel-body">
            <div class="clearfix text-center m-t" style=" margin-bottom: 20px">
              <div class="inline">
                <div style="height: 134px; line-height: 134px; margin-bottom: 40px" class="easyPieChart">
                  <div class="thumb-xl">
                  <?php 
                if(!empty($student_details['profile_image'])){
                  $profile_image = "assets/$_SESSION[sch_id]/".$student_details['profile_image'];
                }else{
                  $profile_image = 'img/a0.jpg';
                }
                ?>
                    <img id="preview" src="<?php echo base_url($profile_image);?>" class="img-circle" alt="..." width='128' height='128'>
                  </div>
                <canvas width="134" height="134"></canvas>
                </div>
                <div style="height:0px;overflow:hidden">
            <input type="file" id="passport" name="passport" onchange="readURL(this);" />
          </div>

        <div class="bootstrap-filestyle input-group"><input class="form-control pupload"  type="button"> <span tabindex="0" class="group-span-filestyle input-group-btn"><label for="filestyle-0" class="btn btn-default pupload"><span class="glyphicon glyphicon-folder-open"></span> Choose file</label></span></div>            </div>                      
            </div>
          </div>
                              
                            </div><br>
                             <div class="form-group">
                              <label class="sr-only" for="sname">Surname </label>
                              <input class="form-control" id="sname" name="sname" placeholder="Surname" style="width: 350px;">

                            </div>
                             <div class="form-group">
                              <label class="sr-only" for="fname">First Name</label>
                              <input class="form-control" id="fname" name="fname" placeholder="First Name" style="width: 350px;">

                            </div>
                             <div class="form-group">
                              <label class="sr-only" for="phone">Phone No</label>
                              <input class="form-control" id="phone" name="phone" placeholder="Phone No" style="width: 350px;">

                            </div>
                            <br><br><br>
                            <div class="form-group">
                              <label class="sr-only" for="Activation">Activation Code</label>
                              <input class="form-control" id="Activation" name="activation_code" placeholder="Activation Code" style="width: 350px;">

                            </div>
                            <span class="ng-scope" ng-controller="ModalDemoCtrl">
                               <button class="btn btn-info btn-addon"><i class="fa fa-check"></i>Activate</button>
                            </span>
                            <span id="helpBlock" class="help-block">Please Enter the activation code from your mail to continue.</span>
                          </form>

                        </div>
                      </div>

                    </div>
                  
                  </div>

                  
                  <div class="clearfix"></div>

                  </div>

                  
        </div>
      </div>
    </div>
    <!-- /content -->

    <style type="text/css">
    .app-footer.wrapper.footer2 {
        margin-left: 0px;
    }
    </style>

    <script type="text/javascript">
  $(document).ready(function(){

    $( ".pupload" ).click(function() {
      $( "#passport" ).click();
    });

     });
  </script> 

  <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result).width(128)
                        .height(128);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>


