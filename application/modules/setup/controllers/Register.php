<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	function __construct(){

		parent::__construct();
		$this->data['title'] = 'School Setup';	
		$this->data['states'] = $this->Corem->get_states();

	}

	public function index()
	{
			// die(print_r($_SESSION));
		if(isset($_SESSION['activated_status'])){
		if($_SESSION['activated_status']== 0){
			redirect("setup/activate_school");
		}elseif($_SESSION['activated_status'] == 1){
			redirect("setup");
		}
		}
		$this->load->template2b('setup/register_school',$this->data);
	}

	public function save_school($edit = FALSE){
		if($edit === FALSE){
			$this->form_validation->set_rules('sch_short_name', 'School Short Name', "trim|required|is_unique[acadah_school.sch_short_name]");
			$this->form_validation->set_rules('aemail', 'Admin Email', 'trim|required|valid_email|is_unique[acadah_users.email]');
			$this->form_validation->set_rules('password', 'Admin Password', 'trim|required');
		}elseif($edit !== FALSE){
			$this->form_validation->set_rules('sch_web', 'School Website', 'trim');
			$this->form_validation->set_rules('gad', 'Government A Website', 'trim');
			$this->form_validation->set_rules('exam_taken', 'Exam', 'trim');
			$this->form_validation->set_rules('name', 'Proprietors Name', 'trim');
			$this->form_validation->set_rules('phone', 'Proprietors Phone', 'trim');
			$this->form_validation->set_rules('about_sch', 'About School', 'trim');
			$this->form_validation->set_rules('pop_range', 'Population Range', 'trim|required');
			$this->form_validation->set_rules('sch_religion', 'School Religion', 'trim|required');
			$this->form_validation->set_rules('sch_hostel', 'School Hostel', 'trim|required');
			$this->form_validation->set_rules('sch_facilities', 'School Facilities', 'trim|required');
		}
		$this->form_validation->set_rules('sch_full_name', 'School Full Name', "trim|required");
		$this->form_validation->set_rules('sch_motto', 'School Motto', 'trim|required');
		$this->form_validation->set_rules('sch_phone1', 'School Phone', 'trim|required');
		$this->form_validation->set_rules('sch_email_add', 'School Email Address', 'trim|required|valid_email');
		$this->form_validation->set_rules('sch_addr', 'School Address', 'trim|required');
		$this->form_validation->set_rules('sstate', 'School State', 'trim|required');
		$this->form_validation->set_rules('lga', 'School LGA', 'trim|required');
		$this->form_validation->set_rules('country', 'School Country', 'trim|required');
		$this->form_validation->set_rules('year_est', 'Year Established', 'trim|required');

		if($this->form_validation->run() === FALSE){
			if($edit === FALSE) {
				$this->register_school();
			}else{
				$this->index();
			}
		}else{
			$odata = array(
				'sch_full_name' => $this->input->post('sch_full_name'),
				'sch_motto' => $this->input->post('sch_motto'),
				'year_est' => $this->input->post('year_est'),
				'sch_email_add' => $this->input->post('sch_email_add'),
				'sch_phone1' => $this->input->post('sch_phone1'),
				'sch_addr' => $this->input->post('sch_addr'),
				'lga' => $this->input->post('lga'),
				'sstate' => $this->input->post('sstate'),
				'country' => $this->input->post('country'),
			);
			if($edit !== FALSE){
				$odata['sch_web'] = $this->input->post('sch_web');
				$odata['gad'] = $this->input->post('gad');
				$odata['exam_taken'] = $this->input->post('exam_taken');
				$odata['about_sch'] = $this->input->post('about_sch');
				$odata['pop_range'] = $this->input->post('pop_range');
				$odata['sch_religion'] = $this->input->post('sch_religion');
				$odata['sch_hostel'] = $this->input->post('sch_hostel');
				$odata['sch_facilities'] = $this->input->post('sch_facilities');

				$udata['lname'] = $this->input->post('name');
				$udata['phone'] = $this->input->post('phone');
				$save = $this->Corem->save_school($udata,$odata,TRUE);
			}elseif($edit === FALSE){
				$odata['sch_short_name'] = $this->input->post('sch_short_name');
				$odata['sch_id'] = strtolower($this->input->post('sch_short_name'));
				$udata = array(
					'email' => $this->input->post('aemail'),
					'pass' => md5($this->input->post('password')),
					'user_type' => 'superadmin'
				);
				$udata['sch_id'] = strtolower($this->input->post('sch_short_name'));
				$save = $this->Corem->save_school($udata,$odata);
			}
			if ($save !== TRUE) {
				$this->session->set_flashdata('failed', $save);
			} elseif ($save === TRUE) {

				$burl = base_url('/login');
				//What Should Happen If the Condition was met
				$this->session->set_flashdata('success', "School Registered Successfully, Kindly Check Your Email($_SESSION[email]) for the activation code <b>($_SESSION[activation_code])</b> <a href='$burl'>Login Here</a>");
				if($edit === FALSE){
				redirect('setup/register');}
				$this->session->set_flashdata('success', "School Details Update Successfully!");
				redirect('setup');
			}
			if($edit === FALSE) {
				$this->register_school();
			}else{
				$this->index();
			}
		}
	}

		
}
