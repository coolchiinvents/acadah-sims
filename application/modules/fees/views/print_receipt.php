			<section>
				<div class="col-sm-12 text-center">
					<h4><span class="h4">STUDENT PAYMENT RECEIPT</span></h4><br>
				</div>
				<div class="col-md-12"> 
						<div class="col-xs-12">

							<div class="">
								<table class="table  table-hover m-b" style="margin-bottom: 40px">
						                <thead>
						                  	<tr style="background:#D9EDF7">
												<th width="">Name: <?php echo $payment_tranx['lname'].' '.$payment_tranx['fname'] ?> </th>
												<th width="">Class: <?php echo $payment_tranx['class_details'] ?></th>												
											</tr>
						                </thead>
						            </table>
								<section class="panel panel-default">
						            <div class="table-responsive">
						             

						              <table class="table table-striped table-bordered table-hover b-t b-light">
						              <?php //print_r($payment_tranx); ?>
						                <tbody>
											<tr>
												<td> Amount Paid:</td>															
												<td> <b> N <?php echo number_format($payment_tranx['trans_amount_paid']); ?> </b></td>
												<td rowspan="8" width="200px" style="padding: 50px">
												<span ><center>
												<img src="<?php echo base_url('img/qr.jpg') ?>" alt="" width="150" height="150"></center>
												</span>
												</td>
											</tr>
											<tr>
												<td> Date Paid:</td>															
												<td> <?php echo $payment_tranx['timestamp'] ?></td>
											</tr>
											<tr>
												<td> Payment Type:</td>															
												<td> <?php echo $payment_tranx['payment_type'] ?></td>
											</tr>
											<tr>
												<td> Bank Account:</td>															
												<td> <?php //echo $payment_tranx['class_details'] ?></td>
											</tr>
											<tr>
												<td> Term:</td>															
												<td> <?php echo $payment_tranx['term_name'] ?> </td>
											</tr>
											<tr>
												<td> Session:</td>															
												<td> <?php echo $payment_tranx['session_name'] ?></td>
											</tr>
											<tr>
												<td> Remark:</td>															
												<td> <?php echo $payment_tranx['comment'] ?></td>
											</tr>
						                </tbody>
						              </table>
						            </div>
						          </section>
							</div>

							<div class="pull-left m-t">
						        <p>Paid By : <span class="h4"><?php echo $payment_tranx['stafftitle'].' '.$payment_tranx['stafflname'].' '.$payment_tranx['stafffname'] ?></span></p>
						        <p>Paid For : <span class="h4"><?php echo $payment_tranx['lname'].' '.$payment_tranx['fname'] ?></span></p>
						        <p>Payment Id : <span class="h4"><?php echo strtoupper($payment_tranx['user_id'].'-'.$payment_tranx['transaction_id']); ?></span></p>
						        <p>Approaved By : <span class="h4">Mr. Ayobanjo (principal)</span></p>
						    </div>
						</div>
					</div>
			</section>
		</div>
		
	</body>
</html>

<style type="text/css">
	/* h4{ font-weight: bold } */
	.h4{ font-weight: bold }
</style>