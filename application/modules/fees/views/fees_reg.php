<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
 <!-- content -->
    <div class="app-content">
      <div ui-butterbar></div>
      
      <a href class="off-screen-toggle hide" data-toggle="class:off-screen" data-target=".app-aside" ></a>
      <div class="app-content-body fade-in-up">
        <!-- COPY the content from "tpl/" -->
       
		<div class="bg-light lter b-b wrapper-md">
		  <span class="pull-right form-inline">
        <form action="" method="post">
         <select name="ts_id" class="form-control">
          <?php foreach ($ts_details as $ts_detail) {?>
             <option <?php echo ($ts_detail['ts_id'] == $ts_id)? 'selected': '' ;?>  
             value="<?php echo $ts_detail['ts_id'];?>">
             <?php echo $ts_detail['term_name']." - ".$ts_detail['session_name']; ?>
            </option>
            <?php }?>
        </select>
        <select id="payment_type" name="fees_id" class="form-control" required="">
            <option value="0">Select Payment Name</option>
            <?php $n = 1; foreach ($fees_names as $fee) { ?>
              <option value="<?php echo $fee['fees_id']; ?>" 
              <?php echo($fee['fees_id'] == $feeid)? 'selected': '';?>>
              <?php echo $fee['fees_name']; ?></option>
            <?php } ?>
          </select>
          <button class="btn btn-info" name="filter" value="filter" type="submit">Go!</button>
        </form>
      </span>
		  <h1 class="m-n font-thin h3">Fees Register and Fees Categories</h1>
	
	</div>
		<div class="wrapper-md">
      <div class="col-sm-12 m-b-lg text-center"><!-- 
        <a href="#searchrange" class="btn btn-lg btn-primary btn-addon m-b-xs" type="button" data-toggle="modal"><i class="fa fa-search"></i>Search Range</a> -->
       <button class="btn btn-lg btn-info btn-addon m-b-xs"><span> No of Fees:</span> <?php echo number_format(count($bills_tranx)); ?> </button>
       <button class="btn btn-lg btn-info btn-addon m-b-xs"><span>Total:</span> N<?php echo number_format($bills_sum); ?></button>
       <button class="btn btn-lg btn-info btn-addon m-b-xs"><span>Total Paid:</span> N<?php echo number_format($bills_paid); ?></button>
       <button class="btn btn-lg btn-danger btn-addon m-b-xs"><span>Total Unpaid:</span> N<?php echo number_format($bills_unpaid); ?></button>
      </div>
            <!-- <div class="form-group col-sm-12">
                <div class="table-responsive">
                  <div class="panel panel-default">
                    <table class="table table-striped b-t b-light">
                      <thead class="panel-heading">
                        <tr>
                          <th>Date</th>
                          <th>Start Date - End Date</th>
                          <th>Term - Session</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody class="panel-body">
                        <tr>
                          <td class="form-inline">
                          <label class="i-checks">
                            <input name="radio" id="radio" type="radio" value="">
                            <i></i>
                          </label>
                           <select class="form-control">
                              <option value="1">Today</option>
                              <option value="1">Yesterday</option>
                              <option value="1">This week</option>
                              <option value="1">Last Week</option>
                              <option value="1">This Month</option>
                              <option value="1">Last Month</option>
                              <option value="1">This Year</option>
                              <option value="1">Last Year</option>
                            </select>
                          </td>
                          <td class="form-inline">
                            <label class="i-checks">
                              <input name="radio" id="radio" type="radio" value="">
                              <i></i>
                            </label>
                            <input class="form-control m-r-sm m-b-sm" required="" type="text">
                        
                            <input class="form-control m-b-sm" type="text" requiredss="">
                          </td>
                          <td class="form-inline ">
                            <label class="i-checks">
                              <input name="radio" id="radio" type="radio" value="">
                              <i></i>
                            </label>
                            <select class="form-control m-r-sm m-b-sm">
                              <option value="1">All Term 2012/2013</option>
                              <option value="1">1st Term 2012/2013</option>
                              <option value="1">2nd Term 2012/2013</option>
                              <option value="1">3rd Term 2012/2013</option>
                            </select>
                          </td>
                          <td><button class="btn btn-info">Go</button></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
            </div> -->
      			<div class="clearfix"></div>
                  <div class="col-sm-12">
                      <div class="panel panel-default">
                            <thead class="panel-heading">
                              <header class="panel-heading">
                              <span class="h3"> STUDENT FEE REGISTER </span>
                                <span class=" pull-right form-inline ">
                                    <input class="form-control input-sm m-b" placeholder="Search" type="text">
                                    <a class="btn btn-sm btn-info m-b"> Total: N<?php echo number_format($bills_sum); ?></a>
                                </span>
                              </header>
                              <div class="row wrapper">
                                <div class="col-xs-6 m-b-xs">
                                  <!--<span class="h4 text-info">From 02-Feb-2016 To 02-Mar-2016</span>  -->

                                  </div>
                                <div class="col-xs-6 text-right">                         
                                  <button class="btn btn-dark btn-sm"><i class="fa fa-print"></i> Print</button>
                                  <!-- <button class="btn btn-sm btn-success"><i class="fa fa-external-link"></i>  Excel</button> -->

                                  <select class="input-sm form-control w-xxs inline v-middle">
                                    <option value="">10</option>
                                    <option value="">20</option>
                                    <option value="">50</option>
                                    <option value="">100</option>
                                    <option value="">200</option>
                                  </select>
                                </div>
                              </div>

                              <div class="table-responsive">
                                <table class="table table-striped b-t b-light">  
                                <tr>
                                    <th class="th-sortable" data-toggle="class">No.</th>
                                    <th>Date</th>
                                    <th>Student Name</th>
                                    <th>Level</th>
                                    <th>Term/Session</th>
                                    <th>Fee</th>
                                    <th>Fee Amount</th>
                                    <th>Amount Paid</th>
                                    <th>Status</th>
                                 </tr>
                                </thead>
                                <tbody class="panel-body">
                                  <?php $n = 0; foreach ($bills_tranx as $bil) { ?>
                                      <tr>
                                        <td><?php echo ++$n; ?></td>
                                        <td><?php echo $bil['timestamp']; ?></td>
                                        <td><?php echo $bil['lname']; ?> - <?php echo $bil['fname']; ?></td>
                                        <td><?php echo $bil['class_details']; ?></td>
                                        <td><?php echo $bil['term_name']; ?> <?php echo $bil['session_name']; ?></td>
                                        <td><?php echo $bil['fees_shortname']; ?></td>
                                        <td>₦<?php echo $bil['amount'];?></td>
                                        <td><?php echo $bil['price']; ?></td>
                                        <td><?php echo ($bil['paid'] == 1)? '<span class="text-success">Paid</span>': '<span class="text-danger">Unpaid</span>';?></td>
                                        <!--<td>₦</td>-->
                                      </tr>
                                      <?php } ?>
                     
                                </tbody>
                              </table>
                          </div>
                        </div>
              		
                  </div>
                </div>
               </div>



<style type="text/css">
  .step-content {
    min-height: 400px !important;
}
</style>