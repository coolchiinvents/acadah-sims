<?php defined('BASEPATH') OR exit('No direct script access allowed');  ?>

<?php //echo lang('sales_giftcard_number')." <small> -  ".lang('sales_giftcard_number'); ?> 
 <!-- content -->
    <div class="app-content">
      <div ui-butterbar></div>
      <a href class="off-screen-toggle hide" data-toggle="class:off-screen" data-target=".app-aside" ></a>
      <div class="app-content-body fade-in-up">
        <!-- COPY the content from "tpl/" -->
       
		<div class="bg-light lter b-b wrapper-md">
		  <span class="pull-right form-inline" >
		  	
            <a href="<?php echo(base_url($module.'/settings'));?>" class="btn btn-default btn-addon  "><i class="fa fa-gear"></i> <?php echo(ucfirst($module));?> Settings </a>
		  </span>
		  <h1 class="m-n font-thin h3"><?php echo(ucfirst($module));?> Records</h1>
		  <small> Manage Oustanding, Discount, Fees and Fees Categories. </small>
		</div>
		<div class="wrapper-md">
			
            <?php
			if(!empty($_SESSION['failed'])){?>
				<div class="alert alert-danger fade in widget-inner">
					<button type="button" class="close" data-dismiss="alert">x</button>
					<?php echo $_SESSION['failed']; ?>
				</div>
			<?php }elseif(!empty($_SESSION['success'])){?>
				<div class="alert alert-success fade in widget-inner">
					<button type="button" class="close" data-dismiss="alert">x</button>
					<?php echo $_SESSION['success'];?>
				</div>
			<?php }?>
			

            <div class="row m-b doc-buttons">
 				<div class="col-md-7">
 					<form class="form-inline" action="<?php echo base_url('fees/fees_bursary'); ?>" role="form" method="post">
		          		<div class="form-group m-b">
		          			<input type="text" name="" class="form-control" placeholder="Search">
		          			 <select name="level_id" class="form-control">
		          			 	<option value="all">
                                  All
                                </option>
                              	<?php foreach ($app_levels_school as $level) { ?>
                                 <option value="<?php echo $level['level_id']; ?>">
                                  <?php echo $level['class_name']; ?> 
                                </option>
                                <?php } ?>
                            </select>
		      		        <select name="ts_id" class="form-control">
                              <?php
                              foreach ($all_ts_details as $ts_detail) {
                                ?>
                                 <option <?php echo ($ts_detail['ts_id'] == $ts_id)? 'selected': '' ;?>  
                                 value="<?php echo $ts_detail['ts_id'];?>">
                                 <a href="<?php echo current_url(); ?>"><?php echo $ts_detail['term_name']." - ".$ts_detail['session_name']; ?> </a>
                                </option>
                                <?php
                              }?>
                            </select>
		      		          <input type="hidden" name="role_id">
			                  <button class="btn btn-info" name="filter" value="filter" type="submit">Go!</button>
			            </div>
			         </form>
	            </div>              
              <div class="col-md-5">
              	<div class="btn-group pull-right">
          			<a href="gen_mainbil" class="btn btn-success"><i class="fa fa-refresh"></i> GENERATE FEES</a>
                </div>
              </div>
             
            </div>
			<section class="panel panel-default">
	            <header class="panel-heading">
	              Student Payment Records Table
	            </header>

	              <div class="table-responsive">
	              <table class="table table-striped b-t b-light" id="list">
	                <thead>
	                  <tr>
	                    <th class="th-sortable" data-toggle="class">No.</th>
	                    <th>Fullname </th>
	                    <th>Class</th>
	                    <th>Stu Cat</th>
	                    <th>Generic Main Bill</th>
	                    <th>Assigned Main Bill</th>
	                    <!-- <th>Credit</th> -->
	                    <th colspan="2">Action</th>
	                  </tr>
	                </thead>
	                <tbody>
						<?php
						foreach($students as $student) {
							if(empty($i)){
								$i = 1;
							}
							?>
							<tr>
								<td><?php echo $i; ?></td>
								<td><?php echo strtoupper($student['lname']); ?> <?php echo strtoupper($student['fname']); ?></td>
								<td><?php echo strtoupper($student['class_details']); ?></td>
								<td><?php echo strtoupper($student['fees_cat']); ?></td>
								<td><?php echo number_format($student['amount']); ?></td>
								<td><?php echo number_format($student['price']); ?></td>
								<td><a href="<?php echo base_url('fees/process/'.$student['user_id']); ?>" class="btn btn-xs btn-warning"><i class="fa fa-dollar"></i> Generate Bill </a>
	                            <!-- <a href="#" class="btn btn-xs btn-info"> Pay History</a></td> -->
							</tr>
							<?php
								$i++;
						}
						?>

	                </tbody>
	              </table>
	            </div>
	            <footer class="panel-footer">
			      <div class="row">
			        <div class="col-sm-4 hidden-xs">
			          <!-- <select class="input-sm form-control w-sm inline v-middle">
			            <option value="0">Bulk action</option>
			            <option value="1">Delete selected</option>
			            <option value="2">Bulk edit</option>
			            <option value="3">Export</option>
			          </select>
			          <button class="btn btn-sm btn-default">Apply</button>  -->                 
			        </div>
			        <div class="col-sm-4 text-center">
			          <small class="text-muted inline m-t-sm m-b-sm">showing 20-30 of 50 items</small>
			        </div>
			        <div class="col-sm-4 text-right text-center-xs">                
			          <ul class="pagination pagination-sm m-t-none m-b-none">
			            <li><a href=""><i class="fa fa-chevron-left"></i></a></li>
			            <li><a href="">1</a></li>
			            <li><a href="">2</a></li>
			            <li><a href="">3</a></li>
			            <li><a href="">4</a></li>
			            <li><a href="">5</a></li>
			            <li><a href=""><i class="fa fa-chevron-right"></i></a></li>
			          </ul>
			        </div>
			      </div>
			    </footer>
	          </section>
		
		
    </div>
  </div>
 </div>