			<section>
				<div class="col-sm-12 text-center">
					<h4>STUDENT PAYMENT BILL</h4><br>
				</div>
				<div class="col-md-12"> 
						<div class="col-xs-12">
							<div class="">
							<table class="table  table-hover m-b" style="margin-bottom: 40px">
						                <thead>
						                  	<tr style="background:#D9EDF7">
												<th width="">Name: <?php echo $stu_fees_info['lname'].' '.$stu_fees_info['fname'] ?> </th>
												<th width="">Class: <?php echo $stu_fees_info['class_details'] ?></th>	
												<th width="">Term: <?php echo($this->session->userdata['ts_details']['term_name']).' - '.$this->session->userdata['ts_details']['session_name']  ?></th>												
											</tr>
						                </thead>
						            </table>
								<section class="panel panel-default">
						            <div class="table-responsive">
						              <table class="table  table-hover b-t b-light">
						                <thead>
						                  	<tr style="background:#D9EDF7">
												<th width="30px">No</th>
												<th width="">Item Name</th>
												<th width="100px">Amount</th>
												<th width="100px">Discount</th>
												<th width="100px">Total</th>
												<th width="150px">Term/Session</th>
												<th width="10px">Pay</th>
												
											</tr>
						                </thead>
						                <tbody>

											<?php $n =0;$sumtotalbil = 0;
											 foreach ($stu_fees_info['bills'] as $bill) {?>
											
											<tr>
												<td><?php echo ++$n; ?>.</td>	
												<td><?php echo $bill['fees_shortname']; ?></td>		
												<td>N<span class="amount"><?php echo($bill['amount']) ; ?></span></td>
												<td><?php echo number_format($bill['discount']) ; ?></td>
												<td><?php echo($totalbil = $bill['amount'] - $bill['discount']) ; $sumtotalbil += $totalbil; ?></td>
												<td><?php echo($bill['term_name']).' - '.$bill['session_name']; ?></td>
												<td><?php echo ($bill['paid']==1)? '<a class="text-success""> Paid</a>': 'Unpaid' ;?>
									            </td>
											</tr>

											<?php } ?>

											<tr>
												<td colspan="4" class="text-right h4"> Total Payable:</td>															
												<td id="total" class="h4">N<?php echo number_format($stu_fees_info['sum_bill']) ;?></td>
												<td></td>	
												<td></td>	
											</tr>
						                </tbody>
						              </table>
						            </div>
						          </section>

						          <section class="panel panel-default">
						            <div class="table-responsive">
						              <table class="table table-striped b-t b-light">
						                <tbody>					                  
										<tr id="reg_item_top">
										   <td  class="text-right h4"> Total Extra: N<?php echo number_format($stu_fees_info['sum_optional_bill']) ; ?></td>
											<td class="text-right h4"> Total Paid:</td>															
											<td id="total" class="h4">N<?php echo number_format($stu_fees_info['paid']) ; ?></td>
											<td class="text-right h4"> Total Balance:</td>															
											<td id="total" class="h4">N<?php echo number_format($stu_fees_info['balance']) ; ?></td>
										</tr>
						                </tbody>
						              </table>					              
						            </div>
						          </section>
							</div>
						</div>
					</div>
			</section>
		</div>
		
	</body>
</html>