			<?php //phpinfo(); 
			//print_r($students); die(); ?>
			<section>
				<div class="col-sm-12 text-center">
					<h4><b>STUDENT LIST</b></h4><br>
				</div>
				<!-- <div class="col-sm-12">
				<table class="table table-hover">
					<thead>
						<tr style="background:#D9EDF7">
							<th>Session:  <?php  print_r($ts_details['session_name']); ?></th>
							<th>Term:  <?php  print_r($ts_details['term_name']); ?></th>
							<th>Level:  <?php  print_r($level_details['class_name']); ?></th>
						</tr>
					</thead>
				</table>
				</div> -->
				<div class="col-sm-12">
	              <section class="panel panel-default" ng-controller="listdata">
		            <header class="panel-heading">
		              Student Records Table
		            </header>
		            <div class="">
		              <table class="table table-striped b-t b-light" id="list">
		                <thead>
		                  <tr>
		                    <th>No.</th>
		                    <th>User ID</th>
		                    <th>Admission</th>
		                    <th>Surname</th>
		                    <th>Firstname</th>
		                    <th>Middle name</th>
		                    <th>Gender</th>
		                    <th>Class</th>
		                    <th></th>
		                  </tr>
		                </thead>
		                <tbody>
		                <?php $n = 1; foreach ($students as $stu) {?>
							<tr>
								<td><?php echo $n++; ?></td>
								<td><a class="text-info" href="<?php echo base_url(); ?>students/edit/<?php echo $stu['user_id']; ?>"><?php echo $stu['user_id']; ?></a></td>
								<td><?php echo $stu['admission_no']; ?></td>
								<td><?php echo $stu['lname']; ?></td>
								<td><?php echo $stu['fname']; ?></td>
								<td><?php echo $stu['mname']; ?></td>
								<td><?php echo $stu['gender']; ?></td>
								<td><?php echo $stu['class_details']; ?></td>
								<td></td>
							</tr>
						<?php } ?>
		                </tbody>
		              </table>
		            </div>
	          	  </section>
	            </div>
			</section>
		</div>
		
	</body>
</html>