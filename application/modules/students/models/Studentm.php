<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Studentm extends CI_Model{

	public function add_parent($parent_id,$id){
		$data = array(
				'parent_id' => $parent_id,
				'student_id' => $id
			);
			return $this->db->on_duplicate('acadah_parent_student',$data);
	}

	public function get_all_students(){
		$sch_id = $_SESSION['sch_id'];
		$this->db->select('stu_cat, admission_no, users.status, user_id, user_type, gender, fname, lname, mname, dob, address, state, phone, email, nationality, hometown, lg, state_of_origin, profile_image, class_details, cd.level_id');
		$this->db->from('users');
		//get ts_details
	    $this->db->join('student_session ss', "users.user_id = ss.student_id" );
	    $this->db->join('student st', "users.user_id = st.student_id" );
	    //AND ss.ts_id = $ts_id
		$this->db->join('class_details cd', 'cd.id = ss.class_details_id');		
		$this->db->join('app_level al', 'al.level_id = cd.level_id');		

		$this->db->where('users.sch_id', $sch_id);
		$this->db->where('users.user_type','student');
		$this->db->order_by('al.sortno');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_student($id,$cdid = NULL){
		if($cdid !== NULL){
			$field = 'admission_class_details_id';
			$id = $cdid;

		}else{
			$field = 'user_id';
		}
		$sch_id = $_SESSION['sch_id'];
		$this->db->where(array('users.sch_id'=>$sch_id,$field=>$id));
		$this->db->join('student st', 'st.student_id=users.user_id');
		$this->db->join('class_level cl', "cl.level_id=st.admission_class and cl.sch_id='$sch_id'");
        $this->db->join('class_details', "class_details.id=st.admission_class_details_id","Left");
		$this->db->from('users');
		$query = $this->db->get();
		if($cdid !== NULL){
			return $query->result_array();
		}
		//print_r($query->row_array());die();
		return $query->row_array();
	}

	public function get_parent($id){
		$sch_id = $_SESSION['sch_id'];

		$this->db->from('parent_student');
		$this->db->join('parent', 'parent.parent_id = parent_student.parent_id ');
		$this->db->join('users', 'users.user_id = parent.parent_id ');
		$this->db->where(array('users.sch_id'=>$sch_id,'student_id'=>$id));

		$query = $this->db->get();
		return $query->result_array();

	}
	public function edit_student($table,$udata,$odata,$id){
		$query = $this->db->update('acadah_users',$udata,array('user_id'=>$id));
		if($query){
			$id = explode('-',$id);
			$query = $this->db->update($table,$odata,array('student_id'=>$id[1]));
			return $query;
		}
		die();
	}

	//tocheckagainlater
		 function list_fees_cat($sch_id = null){
			 $this->db->from('fees_cat fc' );
			 $this->db->where('fc.sch_id', $sch_id);
			 $this->db->or_where('fc.sch_id', 'acadah');
			 $this->db->where('fc.active', 'yes');
			 $query =  $this->db->get();
			  //print_r($query->result_array());
			 return $query->result_array();
		 }


}














?>